package com.example.torneioapp.models;

import com.orm.SugarRecord;

public class Partida extends SugarRecord<Partida> {
    Competidor jogador1, jogador2;
    int golsJ1, golsJ2;
    int rodada;

    public Competidor getJogador1() {
        return jogador1;
    }

    public void setJogador1(Competidor jogador1) {
        this.jogador1 = jogador1;
    }

    public Competidor getJogador2() {
        return jogador2;
    }

    public void setJogador2(Competidor jogador2) {
        this.jogador2 = jogador2;
    }

    public int getGolsJ1() {
        return golsJ1;
    }

    public void setGolsJ1(int golsJ1) {
        this.golsJ1 = golsJ1;
    }

    public int getGolsJ2() {
        return golsJ2;
    }

    public void setGolsJ2(int golsJ2) {
        this.golsJ2 = golsJ2;
    }

    public int getRodada() {
        return rodada;
    }

    public void setRodada(int rodada) {
        this.rodada = rodada;
    }

    public Partida(Competidor jogador1, Competidor jogador2, int golsJ1, int golsJ2, int rodada) {
        this.jogador1 = jogador1;
        this.jogador2 = jogador2;
        this.golsJ1 = golsJ1;
        this.golsJ2 = golsJ2;
        this.rodada = rodada;
    }

    public Partida(Competidor jogador1, Competidor jogador2, int golsJ1, int golsJ2) {
        this.jogador1 = jogador1;
        this.jogador2 = jogador2;
        this.golsJ1 = golsJ1;
        this.golsJ2 = golsJ2;
    }

    public Partida() {
    }
}
