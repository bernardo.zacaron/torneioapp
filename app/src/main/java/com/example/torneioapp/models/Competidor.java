package com.example.torneioapp.models;

import com.orm.SugarRecord;

public class Competidor extends SugarRecord<Competidor> {
    Long id;
    String nome;
    double pontuacao;
    Time timeEscolhido;
    boolean eliminado;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isEliminado() {
        return eliminado;
    }

    public void setEliminado(boolean eliminado) {
        this.eliminado = eliminado;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(double pontuacao) {
        this.pontuacao = pontuacao;
    }

    public Time getTimeEscolhido() {
        return timeEscolhido;
    }

    public void setTimeEscolhido(Time timeEscolhido) {
        this.timeEscolhido = timeEscolhido;
    }

    public Competidor(String nome, double pontuacao, Time timeEscolhido, boolean eliminado) {
        this.nome = nome;
        this.pontuacao = pontuacao;
        this.timeEscolhido = timeEscolhido;
        this.eliminado = eliminado;
    }

    public Competidor(Long id, String nome, double pontuacao, Time timeEscolhido, boolean eliminado) {
        this.id = id;
        this.nome = nome;
        this.pontuacao = pontuacao;
        this.timeEscolhido = timeEscolhido;
        this.eliminado = eliminado;
    }

    public Competidor() {
    }
}
