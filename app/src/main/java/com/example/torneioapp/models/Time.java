package com.example.torneioapp.models;

import com.orm.SugarRecord;

import java.util.List;

public class Time extends SugarRecord<Time> {

    Long id;
    String nome, escalacao;

    public Time(String nome, String escalacao) {
        this.nome = nome;
        this.escalacao = escalacao;
    }

    public Time(long id, String nome, String escalacao) {
        this.id = id;
        this.nome = nome;
        this.escalacao = escalacao;
    }

    public Time() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEscalacao() {
        return escalacao;
    }

    public void setEscalacao(String escalação) {
        this.escalacao = escalacao;
    }

    @Override
    public String toString() {
        return nome;
    }
}
