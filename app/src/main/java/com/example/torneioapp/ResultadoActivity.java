package com.example.torneioapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.torneioapp.models.Competidor;
import com.example.torneioapp.models.Partida;
import com.orm.SugarRecord;

import java.util.List;

public class ResultadoActivity extends AppCompatActivity {

    Button buttonReiniciar;
    TextView lblVencedor;
    Competidor vencedor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        binding();

        montarTela();

        buttonReiniciar.setOnClickListener(reiniciar());
    }

    private View.OnClickListener reiniciar() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limparBanco();

                Intent itn = new Intent(getApplicationContext(), MainActivity.class);

                Toast.makeText(getApplicationContext(), "GG!", Toast.LENGTH_LONG).show();

                startActivity(itn);
            }
        };
    }

    private void limparBanco() {
        SugarRecord.deleteAll(Competidor.class);
        SugarRecord.deleteAll(Partida.class);
    }

    private void montarTela() {
        //Pegando o único competidor que não foi eliminado
        List<Competidor> lista = Competidor.findWithQuery(Competidor.class, "Select * from Competidor where eliminado = 0");
        vencedor = lista.get(0);

        lblVencedor.setText(vencedor.getNome());
    }

    private void binding() {
        lblVencedor = findViewById(R.id.lblVencedor);
        buttonReiniciar = findViewById(R.id.buttonReiniciar);
    }
}