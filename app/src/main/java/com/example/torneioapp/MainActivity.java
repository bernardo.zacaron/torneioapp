package com.example.torneioapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.torneioapp.models.Competidor;
import com.example.torneioapp.models.Partida;
import com.example.torneioapp.models.Time;
import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button buttonAddCompetidor, buttonIniciarTorneio;
    TextView lblContadorCompetidores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        binding();

        alimentarTabelaTimes();

        buttonAddCompetidor.setOnClickListener(abrirTelaAddCompetidor());
        buttonIniciarTorneio.setOnClickListener(iniciarTorneio());
    }

    private View.OnClickListener iniciarTorneio() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent itn = new Intent(getApplicationContext(), Fase1Activity.class);
                startActivity(itn);
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();

        checkQntdCompetidores();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        limparBanco();
    }

    private void checkQntdCompetidores() {
        List<Competidor> competidores = new ArrayList<>();
        competidores = Competidor.listAll(Competidor.class);

        if(competidores != null)
            lblContadorCompetidores.setText("Jogadores: " + String.valueOf(competidores.size()) + "/4");

        if(competidores.size() >= 4){
            buttonAddCompetidor.setEnabled(false);
            buttonIniciarTorneio.setEnabled(true);
        }
    }

    private View.OnClickListener abrirTelaAddCompetidor() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent itn = new Intent(getApplicationContext(), AdicionarCompetidorActivity.class);
                startActivity(itn);
            }
        };
    }

    private void alimentarTabelaTimes() {

        List<Time> times = Time.listAll(Time.class);
        if(times.isEmpty() || times.size()<=2) {
            String escalacao1 = "Vanderlei, Léo Matos, Miranda, Leandro Castan, Zeca, Romulo, Juninho, Marquinhos, Gabriel, Léo Jabá, Morato";
            String escalacao2 = "Diego Alves, Isla, Rodrigo Caio, Gustavo Henrique, Renê, Willian Arão, Andreas Pereira, Everton Ribeiro, Vitinho, Michael, Gabigol";
            String escalacao3 = "Everson, Guga, athan Silva, Réver, Dodô, Allan, Jair, Zaracho, Nacho, Hulk, Keno, Diego Costa";
            String escalacao4 = "Neuer, Pavard, Upamecano, Süle, Davies, Kimmich, Goretzka, Sabitzer, Gnabry, Müller, Sané, Lewandowski";
            String escalacao5 = "Ter Stegen, Mingueza, Araujo, Pique, Dest, Gavi, Busquets, Sergi Roberto, Coutinho, Luuk de Jong, Memphis Depay";
            String escalacao6 = "Donnarumma, Hakimi, Kehrer, Kimpembe, Mendes, Gueye, Verratti, Wijnaldum, Mbappe, Icardi, Rafinha";

            Time time1 = new Time(1, "Vasco da Gama", escalacao1);
            Time time2 = new Time(2, "Flamengo", escalacao2);
            Time time3 = new Time(3, "Atletico Mineiro", escalacao3);
            Time time4 = new Time(4, "Bayern de Munique", escalacao4);
            Time time5 = new Time(5, "Barcelona", escalacao5);
            Time time6 = new Time(6, "Paris Saint-Germain", escalacao6);

            time1.save();
            time2.save();
            time3.save();
            time4.save();
            time5.save();
            time6.save();
        }
    }

    private void limparBanco() {
        SugarRecord.deleteAll(Competidor.class);
        SugarRecord.deleteAll(Partida.class);
    }

    private void binding() {
        buttonAddCompetidor = findViewById(R.id.buttonAddCompetidor);
        buttonIniciarTorneio = findViewById(R.id.buttonIniciarTorneio);
        lblContadorCompetidores = findViewById(R.id.lblContadorCompetidores);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.times:
                Intent itn = new Intent(getApplicationContext(), TimesActivity.class);
                startActivity(itn);
                return true;
            case R.id.regras:
                Intent itn2 = new Intent(getApplicationContext(), RegrasActivity.class);
                startActivity(itn2);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}