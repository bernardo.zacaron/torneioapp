package com.example.torneioapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.torneioapp.models.Competidor;
import com.example.torneioapp.models.Partida;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;

public class Fase2Activity extends AppCompatActivity {

    TextView lblFinalista1, lblFinalista2;
    TextInputEditText textGolsF1, textGolsF2;
    Button buttonFinalizarTorneio;

    Competidor finalista1, finalista2;
    Partida partida = new Partida(finalista1, finalista2, 0, 0, 2);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fase2);

        binding();
        
        definirFinalistas();

        montarTela();

        buttonFinalizarTorneio.setOnClickListener(validarCampos());
    }

    private View.OnClickListener validarCampos() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textGolsF1.getText().toString().isEmpty()) {
                    textGolsF1.setError("A quantidade de gols marcadas deve ser informada");
                    textGolsF1.requestFocus();
                    return ;
                }
                if(Integer.parseInt(textGolsF1.getText().toString()) < 0) {
                    textGolsF1.setError("A quantidade de gols não pode ser negativa");
                    textGolsF1.requestFocus();
                    return ;
                }
                if(Integer.parseInt(textGolsF1.getText().toString()) > 50) {
                    textGolsF1.setError("Você não é tão bom assim!");
                    textGolsF1.requestFocus();
                    return ;
                }

                if(textGolsF2.getText().toString().isEmpty()) {
                    textGolsF2.setError("A quantidade de gols marcadas deve ser informada");
                    textGolsF2.requestFocus();
                    return ;
                }
                if(Integer.parseInt(textGolsF2.getText().toString()) < 0) {
                    textGolsF2.setError("A quantidade de gols não pode ser negativa");
                    textGolsF2.requestFocus();
                    return ;
                }
                if(Integer.parseInt(textGolsF2.getText().toString()) > 50) {
                    textGolsF2.setError("Você não é tão bom assim!");
                    textGolsF2.requestFocus();
                    return ;
                }

                else{
                    distribuirPontuacao();
                    finalizar();
                }
            }
        };
    }

    private void distribuirPontuacao() {
        partida.setGolsJ1(Integer.parseInt(textGolsF1.getText().toString()));
        partida.setGolsJ2(Integer.parseInt(textGolsF2.getText().toString()));
        partida.save();

        finalista1.setPontuacao(finalista1.getPontuacao() + Integer.parseInt(textGolsF1.getText().toString()) * 0.5);
        finalista1.setPontuacao(finalista1.getPontuacao() - Integer.parseInt(textGolsF2.getText().toString()) * 0.1);
        finalista2.setPontuacao(finalista2.getPontuacao() + Integer.parseInt(textGolsF2.getText().toString()) * 0.5);
        finalista2.setPontuacao(finalista2.getPontuacao() - Integer.parseInt(textGolsF1.getText().toString()) * 0.1);

        if(Integer.parseInt(textGolsF1.getText().toString()) > Integer.parseInt(textGolsF2.getText().toString())){
            finalista1.setPontuacao(finalista1.getPontuacao() + 3);
            finalista1.setEliminado(false);
            finalista2.setEliminado(true);
        }
        if(Integer.parseInt(textGolsF1.getText().toString()) < Integer.parseInt(textGolsF2.getText().toString())){
            finalista2.setPontuacao(finalista2.getPontuacao() + 3);
            finalista1.setEliminado(true);
            finalista2.setEliminado(false);
        }
        if(Integer.parseInt(textGolsF1.getText().toString()) == Integer.parseInt(textGolsF2.getText().toString())){
            finalista1.setPontuacao(finalista1.getPontuacao() + 1);
            finalista2.setPontuacao(finalista2.getPontuacao() + 1);

            if(finalista1.getPontuacao() > finalista2.getPontuacao()){ //Criterio de desempate = o jogador com maior pontuação vence
                finalista1.setEliminado(false);
                finalista2.setEliminado(true);
            }else{
                finalista1.setEliminado(true);
                finalista2.setEliminado(false);
            }
        }
        finalista1.save();
        finalista2.save();
    }

    private void finalizar() {
        Intent itn = new Intent(getApplicationContext(), ResultadoActivity.class);
        startActivity(itn);
    }

    private void montarTela() {
        lblFinalista1.setText(finalista1.getNome());
        lblFinalista2.setText(finalista2.getNome());
    }

    private void binding() {
        lblFinalista1 = findViewById(R.id.lblFinalista1);
        lblFinalista2 = findViewById(R.id.lblFinalista2);
        textGolsF1 = findViewById(R.id.textGolsF1);
        textGolsF2 = findViewById(R.id.textGolsF2);
        buttonFinalizarTorneio = findViewById(R.id.buttonFinalizarTorneio);
    }

    private void definirFinalistas() {
        List<Competidor> naoEliminados = Competidor.findWithQuery(Competidor.class, "Select * from Competidor where eliminado = 0");
        finalista1 = naoEliminados.get(0);
        finalista2 = naoEliminados.get(1);
    }
}