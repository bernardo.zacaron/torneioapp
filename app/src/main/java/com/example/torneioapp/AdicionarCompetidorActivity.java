package com.example.torneioapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.torneioapp.models.Competidor;
import com.example.torneioapp.models.Time;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;

public class AdicionarCompetidorActivity extends AppCompatActivity {

    Spinner spinnerTime;
    TextInputEditText textNome;
    Button buttonConcluir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adicionar_competidor);

        binding();


        List<Time> times = Time.listAll(Time.class);

        ArrayAdapter<Time> adapter = new ArrayAdapter<Time>(this, android.R.layout.simple_spinner_item, times);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerTime.setAdapter(adapter);

        buttonConcluir.setOnClickListener(salvarCompetidor());
    }

    private Time getTimeSelecionado(){
        Time time = (Time) spinnerTime.getSelectedItem();
        return  time;
    }

    private View.OnClickListener salvarCompetidor() {

        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textNome.getText().toString().isEmpty()) {
                    textNome.setError("O nome do competidor deve ser informado");
                    textNome.requestFocus();
                    return ;
                }else {
                    Competidor competidor = new Competidor(textNome.getText().toString(), 0, getTimeSelecionado(), false);
                    competidor.save();

                    Toast.makeText(getApplicationContext(), "Competidor adicionado. Boa sorte, " + competidor.getNome() + "!", Toast.LENGTH_LONG).show();

                    Intent itn = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(itn);
                }
            }
        };
    }

    private void binding() {
        spinnerTime = findViewById(R.id.spinnerTime);
        textNome = findViewById(R.id.textNome);
        buttonConcluir = findViewById(R.id.buttonConcluir);
    }
}