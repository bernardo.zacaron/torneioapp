package com.example.torneioapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.torneioapp.models.Time;

import java.util.List;
import java.util.stream.Collectors;

public class TimesActivity extends AppCompatActivity {

    TextView time1, time2,time3,time4,time5,time6;
    TextView escalacao1, escalacao2, escalacao3 ,escalacao4 ,escalacao5 ,escalacao6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_times);

        List<Time> todosTimes = Time.listAll(Time.class);

        binding();

        apresentarTimes(todosTimes);
    }

    private void apresentarTimes(List<Time> times) {
        time1.setText(times.get(0).getNome());
        escalacao1.setText("Escalação: "+ times.get(0).getEscalacao());

        time2.setText(times.get(1).getNome());
        escalacao2.setText("Escalação: "+times.get(1).getEscalacao());

        time3.setText(times.get(2).getNome());
        escalacao3.setText("Escalação: "+times.get(2).getEscalacao());

        time4.setText(times.get(3).getNome());
        escalacao4.setText("Escalação: "+times.get(3).getEscalacao());

        time5.setText(times.get(4).getNome());
        escalacao5.setText("Escalação: "+times.get(4).getEscalacao());

        time6.setText(times.get(5).getNome());
        escalacao6.setText("Escalação: "+times.get(5).getEscalacao());

    }

    private void binding() {
        time1 = findViewById(R.id.time1);
        time2 = findViewById(R.id.time2);
        time3 = findViewById(R.id.time3);
        time4 = findViewById(R.id.time4);
        time5 = findViewById(R.id.time5);
        time6 = findViewById(R.id.time6);
        escalacao1 = findViewById(R.id.escalacao1);
        escalacao2 = findViewById(R.id.escalacao2);
        escalacao3 = findViewById(R.id.escalacao3);
        escalacao4 = findViewById(R.id.escalacao4);
        escalacao5 = findViewById(R.id.escalacao5);
        escalacao6 = findViewById(R.id.escalacao6);
    }
}