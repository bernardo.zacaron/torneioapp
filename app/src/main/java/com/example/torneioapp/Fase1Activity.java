package com.example.torneioapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.torneioapp.models.Competidor;
import com.example.torneioapp.models.Partida;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;
import java.util.Random;

public class Fase1Activity extends AppCompatActivity {

    TextView lblCompetidor1, lblCompetidor2, lblCompetidor3, lblCompetidor4;
    TextInputEditText textGolsJ1G1, textGolsJ2G1, textGolsJ1G2, textGolsJ2G2;
    Button buttonProxima;

    List<Competidor> jogadores = Competidor.listAll(Competidor.class);
    int selecionandoJogador=1; //Apontará quando parar de montar a chave
    Competidor j1, j2, j3, j4;

    Partida partida1, partida2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fase1);

        binding();
        montarTela();

        buttonProxima.setOnClickListener(validarCampos());
    }

    private View.OnClickListener validarCampos() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textGolsJ1G1.getText().toString().isEmpty()) {
                    textGolsJ1G1.setError("A quantidade de gols marcadas deve ser informada");
                    textGolsJ1G1.requestFocus();
                    return ;
                }
                if(Integer.parseInt(textGolsJ1G1.getText().toString()) < 0) {
                    textGolsJ1G1.setError("A quantidade de gols não pode ser negativa");
                    textGolsJ1G1.requestFocus();
                    return ;
                }
                if(Integer.parseInt(textGolsJ1G1.getText().toString()) > 50) {
                    textGolsJ1G1.setError("Você não é tão bom assim!");
                    textGolsJ1G1.requestFocus();
                    return ;
                }

                if(textGolsJ2G1.getText().toString().isEmpty()) {
                    textGolsJ2G1.setError("A quantidade de gols marcadas deve ser informada");
                    textGolsJ2G1.requestFocus();
                    return ;
                }
                if(Integer.parseInt(textGolsJ2G1.getText().toString()) < 0) {
                    textGolsJ2G1.setError("A quantidade de gols não pode ser negativa");
                    textGolsJ2G1.requestFocus();
                    return ;
                }
                if(Integer.parseInt(textGolsJ2G1.getText().toString()) > 50) {
                    textGolsJ2G1.setError("Você não é tão bom assim!");
                    textGolsJ2G1.requestFocus();
                    return ;
                }

                if(textGolsJ1G2.getText().toString().isEmpty()) {
                    textGolsJ1G2.setError("A quantidade de gols marcadas deve ser informada");
                    textGolsJ1G2.requestFocus();
                    return ;
                }
                if(Integer.parseInt(textGolsJ1G2.getText().toString()) < 0) {
                    textGolsJ1G2.setError("A quantidade de gols não pode ser negativa");
                    textGolsJ1G2.requestFocus();
                    return ;
                }
                if(Integer.parseInt(textGolsJ1G2.getText().toString()) > 50) {
                    textGolsJ1G2.setError("Você não é tão bom assim!");
                    textGolsJ1G2.requestFocus();
                    return ;
                }

                if(textGolsJ2G2.getText().toString().isEmpty()) {
                    textGolsJ2G2.setError("A quantidade de gols marcadas deve ser informada");
                    textGolsJ2G2.requestFocus();
                    return ;
                }
                if(Integer.parseInt(textGolsJ2G2.getText().toString()) < 0) {
                    textGolsJ2G2.setError("A quantidade de gols não pode ser negativa");
                    textGolsJ2G2.requestFocus();
                    return ;
                }
                if(Integer.parseInt(textGolsJ2G2.getText().toString()) > 50) {
                    textGolsJ2G2.setError("Você não é tão bom assim!");
                    textGolsJ2G2.requestFocus();
                    return ;
                }
                else{
                    distribuirPontuacao();
                    proximaRodada();
                }
            }
        };
    }

    private void distribuirPontuacao() {
        //Salvando o resultado das partidas
        partida1.setGolsJ1(Integer.parseInt(textGolsJ1G1.getText().toString()));
        partida1.setGolsJ2(Integer.parseInt(textGolsJ2G1.getText().toString()));
        partida1.save();
        partida2.setGolsJ1(Integer.parseInt(textGolsJ1G2.getText().toString()));
        partida2.setGolsJ2(Integer.parseInt(textGolsJ2G2.getText().toString()));
        partida2.save();

        //Pontuando os jogadores por gol marcado e tomado
        j1.setPontuacao(j1.getPontuacao() + Integer.parseInt(textGolsJ1G1.getText().toString()) * 0.5);
        j1.setPontuacao(j1.getPontuacao() - Integer.parseInt(textGolsJ2G1.getText().toString()) * 0.1);
        j2.setPontuacao(j2.getPontuacao() + Integer.parseInt(textGolsJ2G1.getText().toString()) * 0.5);
        j2.setPontuacao(j2.getPontuacao() - Integer.parseInt(textGolsJ1G1.getText().toString()) * 0.1);

        j3.setPontuacao(j3.getPontuacao() + Integer.parseInt(textGolsJ1G2.getText().toString()) * 0.5);
        j3.setPontuacao(j3.getPontuacao() - Integer.parseInt(textGolsJ2G2.getText().toString()) * 0.1);
        j4.setPontuacao(j4.getPontuacao() + Integer.parseInt(textGolsJ2G2.getText().toString()) * 0.5);
        j4.setPontuacao(j4.getPontuacao() - Integer.parseInt(textGolsJ1G2.getText().toString()) * 0.1);

        //Pontuando o jogador vencedor da partida
        if(Integer.parseInt(textGolsJ1G1.getText().toString()) > Integer.parseInt(textGolsJ2G1.getText().toString())){
            j1.setPontuacao(j1.getPontuacao() + 3);
            j1.setEliminado(false);
            j2.setEliminado(true);
        }
        if(Integer.parseInt(textGolsJ1G1.getText().toString()) < Integer.parseInt(textGolsJ2G1.getText().toString())){
            j2.setPontuacao(j2.getPontuacao() + 3);
            j1.setEliminado(true);
            j2.setEliminado(false);
        }
        if(Integer.parseInt(textGolsJ1G1.getText().toString()) == Integer.parseInt(textGolsJ2G1.getText().toString())){
            j1.setPontuacao(j1.getPontuacao() + 1);
            j2.setPontuacao(j2.getPontuacao() + 1);

            if(Integer.parseInt(textGolsJ1G2.getText().toString()) % 2 == 0){ //Criterio de desempate aleatório, para definir quem passará para a proxima fase
                j1.setEliminado(false);
                j2.setEliminado(true);
            }
        }

        if(Integer.parseInt(textGolsJ1G2.getText().toString()) > Integer.parseInt(textGolsJ2G2.getText().toString())){
            j3.setPontuacao(j3.getPontuacao() + 3);
            j3.setEliminado(false);
            j4.setEliminado(true);
        }
        if(Integer.parseInt(textGolsJ1G2.getText().toString()) < Integer.parseInt(textGolsJ2G2.getText().toString())){
            j4.setPontuacao(j4.getPontuacao() + 3);
            j3.setEliminado(true);
            j4.setEliminado(false);
        }
        if(Integer.parseInt(textGolsJ1G2.getText().toString()) == Integer.parseInt(textGolsJ2G2.getText().toString())){
            j3.setPontuacao(j3.getPontuacao() + 1);
            j4.setPontuacao(j4.getPontuacao() + 1);

            if(Integer.parseInt(textGolsJ1G1.getText().toString()) % 2 == 0){
                j3.setEliminado(false);
                j4.setEliminado(true);
            }
        }
        j1.save();
        j2.save();
        j3.save();
        j4.save();
    }

    private void proximaRodada(){
        Intent itn = new Intent(getApplicationContext(), Fase2Activity.class);
        startActivity(itn);
    }

    private void montarTela() {
        textGolsJ1G1.requestFocus();
        randomizarChave();
    }

    private void randomizarChave() {

        while(selecionandoJogador <= 4){
            Random r = new Random();
            int numCompetidor = r.nextInt((3-0)+1)+0;

            if(jogadores.get(numCompetidor).isEliminado()==false){
                if(j1 == null){
                    j1 = jogadores.get(numCompetidor);
                    j1.setEliminado(true);
                }else if(j2 == null){
                    j2 = jogadores.get(numCompetidor);
                    j2.setEliminado(true);
                }else if(j3== null){
                    j3 = jogadores.get(numCompetidor);
                    j3.setEliminado(true);
                }else{
                    j4 = jogadores.get(numCompetidor);
                    j4.setEliminado(true);
                }
                selecionandoJogador++;
            }else{
                randomizarChave();
            }
        }
        partida1 = new Partida(j1, j2, 0,0, 1);
        partida2 = new Partida(j3, j4, 0,0, 2);

        exibirInformacoes(partida1, partida2);
    }

    private void exibirInformacoes(Partida partida1, Partida partida2) {
        lblCompetidor1.setText(partida1.getJogador1().getNome());
        lblCompetidor2.setText(partida1.getJogador2().getNome());

        lblCompetidor3.setText(partida2.getJogador1().getNome());
        lblCompetidor4.setText(partida2.getJogador2().getNome());
    }

    private void binding() {
        lblCompetidor1 = findViewById(R.id.lblCompetidor1);
        lblCompetidor2 = findViewById(R.id.lblCompetidor2);
        lblCompetidor3 = findViewById(R.id.lblCompetidor3);
        lblCompetidor4 = findViewById(R.id.lblCompetidor4);
        textGolsJ1G1 = findViewById(R.id.textGolsF2);
        textGolsJ2G1 = findViewById(R.id.textGolsJ2G1);
        textGolsJ1G2 = findViewById(R.id.textGolsJ1G2);
        textGolsJ2G2 = findViewById(R.id.textGolsJ2G2);
        buttonProxima = findViewById(R.id.buttonProxima);
    }
}