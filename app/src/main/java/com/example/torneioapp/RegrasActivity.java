package com.example.torneioapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.torneioapp.models.Competidor;
import com.example.torneioapp.models.Partida;
import com.orm.SugarRecord;

public class RegrasActivity extends AppCompatActivity {

    Button buttonVoltar;
    ImageButton imageButtonSecreto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regras);

        buttonVoltar = findViewById(R.id.buttonVoltarMenuInicial);
        imageButtonSecreto = findViewById(R.id.imageButtonSecreto);

        buttonVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent itn = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(itn);
            }
        });
        imageButtonSecreto.setOnClickListener(new View.OnClickListener() { //maneira alternativa de limpar o banco de dados
            @Override
            public void onClick(View view) {
                SugarRecord.deleteAll(Competidor.class);
                SugarRecord.deleteAll(Partida.class);
            }
        });
    }
}